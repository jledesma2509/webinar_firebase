package com.academiamoviles.firebasewebinarapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.academiamoviles.firebasewebinarapp.databinding.ActivityMainBinding
import com.academiamoviles.firebasewebinarapp.model.Curso
import com.google.firebase.firestore.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val db = FirebaseFirestore.getInstance()
    private val collecionCurso = db.collection("cursos")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegistrar.setOnClickListener {

            registrar()
        }


        binding.btnActualizar.setOnClickListener {

            actualizar()
        }

        binding.btnEliminar.setOnClickListener {

            eliminar()
        }

        binding.btnObtener.setOnClickListener {

            obtener()
        }

    }

    private fun obtener() {

        val cursoRef = collecionCurso.document("hT5lvo8IbXGeOSspbyav")

        /*cursoRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Toast.makeText(this, "${document.data}", Toast.LENGTH_SHORT).show()
                    println("${document.data}")
                } else {
                    Toast.makeText(this, "No existe el documento", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener { exception ->
                Toast.makeText(this, exception.message, Toast.LENGTH_SHORT).show()
            }


        cursoRef.get().addOnSuccessListener { documentSnapshot ->
            val curso = documentSnapshot.toObject(Curso::class.java)
            curso?.let {
                println("Nombre del curso: ${it.nombre} - Frecuencia:${it.frecuencia}")
            }
        }*/

        collecionCurso
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    val curso = document.toObject(Curso::class.java)
                    println(document.id)
                    println(curso.nombre)
                    println(curso.frecuencia)
                }
            }
            .addOnFailureListener { exception ->
                Toast.makeText(this, exception.message, Toast.LENGTH_SHORT).show()
            }


        // Source can be CACHE, SERVER, or DEFAULT.
        //val source = Source.SERVER

        // Get the document, forcing the SDK to use the offline cache
        //cursoRef.get(source)
        //    .addOnSuccessListener { document ->
        //        if (document != null) {
        //            Toast.makeText(this, "${document.data}", Toast.LENGTH_SHORT).show()
        //            println("${document.data}")
        //        } else {
        //            Toast.makeText(this, "No existe el documento", Toast.LENGTH_SHORT).show()
        //        }
        //    }
        //    .addOnFailureListener { exception ->
        //        Toast.makeText(this, exception.message, Toast.LENGTH_SHORT).show()
        //    }
}

private fun eliminar() {

    val cursoRef = collecionCurso.document("2")

    cursoRef.delete().addOnSuccessListener {
        Toast.makeText(this, "Curso eliminado", Toast.LENGTH_SHORT).show()
    }.addOnFailureListener {
        Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
    }
}

private fun actualizar() {

    val nombreCurso = binding.edtCurso.text.toString()
    val frecuencia = binding.edtFrecuencia.text.toString()

    val cursoRef = collecionCurso.document("hT5lvo8IbXGeOSspbyav")

    /*cursoRef
        .update("nombre", nombreCurso)
        .addOnSuccessListener {
            Toast.makeText(this, "Curso actualizado", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
        }

    cursoRef
        .update(mapOf(
            "nombre" to nombreCurso,
            "frecuencia" to frecuencia
        )).addOnSuccessListener {
            Toast.makeText(this, "Curso actualizado", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
        }
-
     */

    // Update the timestamp field with the value from the server
    val updates = hashMapOf<String, Any>(
        "timestamp" to FieldValue.serverTimestamp()
    )

    cursoRef.update(updates)
        .addOnSuccessListener {
            Toast.makeText(this, "Curso actualizado", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
        }
}

private fun registrar() {

    //db.collection("curso").document("2").set(curso)
    val nombreCurso = binding.edtCurso.text.toString()
    val frecuencia = binding.edtFrecuencia.text.toString()

    //Formas de registro
    //1. hashMapOf
    val cursoHashMap = hashMapOf(
        "nombre" to nombreCurso,
        "frecuencia" to frecuencia
    )

    collecionCurso.document("1").set(cursoHashMap).addOnSuccessListener {
        Toast.makeText(this, "Curso registrado", Toast.LENGTH_SHORT).show()
    }.addOnFailureListener {
        Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
    }

    //2 Objeto
    //val curso = Curso(nombreCurso,frecuencia)
    //collecionCurso.document("2").set(curso).addOnSuccessListener{
    //    Toast.makeText(this,"Curso registrado", Toast.LENGTH_SHORT).show()
    //}.addOnFailureListener {
    //    Toast.makeText(this,it.message, Toast.LENGTH_SHORT).show()
    //}

    //3. Objeto con documento predeterminado
    //collecionCurso.add(curso).addOnSuccessListener{
    //    Toast.makeText(this,"Curso registrado", Toast.LENGTH_SHORT).show()
    //}.addOnFailureListener {
    //    Toast.makeText(this,it.message, Toast.LENGTH_SHORT).show()
    //}
}
}