package com.academiamoviles.firebasewebinarapp.model

data class Curso(
    val nombre:String,
    val frecuencia:String) {

    constructor():this("","")
}